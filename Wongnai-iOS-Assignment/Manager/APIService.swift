//
//  APIService.swift
//  Wongnai-iOS-Assignment
//
//  Created by Mohit Gupta on 15/10/21.
//

import Foundation
import UIKit

class APIService :  NSObject {
    private let APIURL = URL(string: "https://api.500px.com/v1/photos?feature=popular&page=1")!
    func apiToGetData(completion : @escaping (Model) -> ()){
        URLSession.shared.dataTask(with: APIURL) { (data, urlResponse, error) in
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    print(json)
                    let jsonDecoder = JSONDecoder()
                    if let response = try? jsonDecoder.decode(Model.self, from: data){
                        completion(response)
                    }
                }
                catch {
                    print(error)
                }
            }
            
        }.resume()
    } 
}



let imageCache = NSCache<AnyObject, AnyObject>()

class ImageLoader: UIImageView {
    
    var imageURL: URL?
    let activityIndicator = UIActivityIndicatorView()
    func loadImageWithUrl(_ url: URL) {
        activityIndicator.color = .darkGray
        addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        imageURL = url
        image = nil
        activityIndicator.startAnimating()
        if let imageFromCache = imageCache.object(forKey: url as AnyObject) as? UIImage {
            self.image = imageFromCache
            activityIndicator.stopAnimating()
            return
        }
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if error != nil {
                print(error as Any)
                DispatchQueue.main.async(execute: {
                    self.activityIndicator.stopAnimating()
                })
                return
            }
            DispatchQueue.main.async(execute: {
                if let unwrappedData = data, let imageToCache = UIImage(data: unwrappedData) {
                    if self.imageURL == url {
                        self.image = imageToCache
                    }
                    imageCache.setObject(imageToCache, forKey: url as AnyObject)
                }
                self.activityIndicator.stopAnimating()
            })
        }).resume()
    }
}
