//
//  TableViewCell.swift
//  Wongnai-iOS-Assignment
//
//  Created by Mohit Gupta on 15/10/21.
//

import UIKit
import SDWebImage
class TableViewCell: UITableViewCell { 
    @IBOutlet weak var lblTitle: UILabel?
    @IBOutlet weak var lblDescription: UILabel?
    @IBOutlet weak var imgLogo: UIImageView?
    @IBOutlet weak var lblVoteCount: UILabel?
    var obj : Photo? {
        didSet {
            lblTitle?.text = obj?.name ?? ""
            lblDescription?.text = obj?.photoDescription ?? ""
            let num = obj?.positiveVotesCount?.withCommas()
            lblVoteCount?.text = "\(num ?? "")"
            imgLogo?.sd_setImage(with: URL(string: (obj?.imageURL?[0] ?? "")), placeholderImage: UIImage(named: "placeholder.png"))
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
