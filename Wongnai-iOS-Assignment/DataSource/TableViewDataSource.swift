//
//  TableViewDataSource.swift
//  Wongnai-iOS-Assignment
//
//  Created by Mohit Gupta on 15/10/21.
//

import Foundation
import UIKit

class  TableViewDataSource<CELL : UITableViewCell,T> : NSObject, UITableViewDataSource {
    
    private var cellIdentifier : String?
    private var items : [T]?
    var configureCell : (CELL, T) -> () = {_,_ in }
    
    
    init(cellIdentifier : String, items : [T], configureCell : @escaping (CELL, T) -> ()) {
        self.cellIdentifier = cellIdentifier
        self.items =  items
        self.configureCell = configureCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if ((indexPath.row % 5) == 0){
            if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? CELL{
                return cell
            }
        }else{
            if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier ?? "", for: indexPath) as? CELL{
                guard let item = self.items?[indexPath.row] else { return UITableViewCell() }
                self.configureCell(cell, item)
                return cell
            }
        }
        
        return UITableViewCell()
        
    }
    
}

