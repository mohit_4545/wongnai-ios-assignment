//
//  ViewController.swift
//  Wongnai-iOS-Assignment
//
//  Created by Mohit Gupta on 15/10/21.
//

import UIKit
import SDWebImage
class ViewController: UIViewController {
    @IBOutlet weak var  TableView: UITableView!
    
    private var viewmodel : ViewModel!
    private var dataSource : TableViewDataSource<TableViewCell,Photo>!
    var indicator =  UIAlertController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NetworkManager.isReachable { _ in
            self.callToViewModelForUIUpdate()
        }
        NetworkManager.isUnreachable { _ in
            // create the alert
            let alert = UIAlertController(title: "Information", message: "Internet Disconnected", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension ViewController{
    
    func callToViewModelForUIUpdate(){
        self.indicator = self.loader()
        self.viewmodel = ViewModel()
        self.viewmodel?.bindViewModelToController = {
            self.updateDataSource()
        }
    }
    
    func updateDataSource(){
        
        self.dataSource = TableViewDataSource(cellIdentifier: "TableViewCell", items: self.viewmodel.obj.photos!, configureCell: { (cell, obj) in
            cell.lblTitle?.text = obj.name
            cell.lblDescription?.text = obj.photoDescription
            let num = obj.positiveVotesCount?.withCommas()
            cell.lblVoteCount?.text = "\(num ?? "")"
            cell.imgLogo?.sd_setImage(with: URL(string: (obj.imageURL?[0] ?? "")), placeholderImage: UIImage(named: "placeholder.png"))
        })
        
        DispatchQueue.main.async {
            self.stopLoader(loader: self.indicator)
            self.TableView.dataSource = self.dataSource
            self.TableView.delegate = self
            self.TableView.reloadData()
        }
    }
    
    func click(){
        ImagePickerManager().pickImage(self){ image in
            //here is the image
        }
    }
}

extension ViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if ((indexPath.row % 5) == 0){
            self.click()
        }
    }
}
