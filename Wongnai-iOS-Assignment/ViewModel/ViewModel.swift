//
//  ViewModel.swift
//  Wongnai-iOS-Assignment
//
//  Created by Mohit Gupta on 15/10/21.
//

import Foundation

class ViewModel : NSObject {
    
    private var apiService : APIService!
    private(set) var obj : Model!{
        didSet {
            self.bindViewModelToController()
        }
    }
    var bindViewModelToController : (() -> ()) = {}
    override init() {
        super.init()
        self.apiService =  APIService()
        callFuncToGetData()
    }
    
    func callFuncToGetData() {
        self.apiService.apiToGetData { (data) in
            self.obj = data
        }
        
        
    } 
 
}
